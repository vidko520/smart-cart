const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const webpush = require('web-push');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());

// Povezava na MongoDB Atlas (ali drugo oddaljeno instanco MongoDB)
mongoose.connect('mongodb+srv://gorenjak:mavrica@cluster0.hbsyog8.mongodb.net/SmartCart', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const User = mongoose.model('User', {
    firstname: String,
    lastname: String,
    email: String,
    username: String,
    password: String,
});

// Konfiguracija za JWT avtentikacijo
const accessTokenSecret = 'access_token_secret';
const refreshTokenSecret = 'refresh_token_secret';
const accessTokenLife = '30m'; // Dostopni žeton velja 30 minut
const refreshTokenLife = '1d'; // Osveževalni žeton velja 1 dan

// Končna točka za registracijo
app.post('/api/register', async (req, res) => {
  const { firstname, lastname, username, password, email } = req.body;
  if (!firstname || !lastname || !username || !password || !email) {
    return res.status(400).json({ message: 'Manjka ime, priimek, uporabniško ime, geslo ali e-pošta' });
  }

  if (password.length < 8) {
    return res.status(400).json({ message: 'Geslo mora biti dolgo vsaj 8 znakov' });
  }

  try {
    const existingUser = await User.findOne({ username });
    if (existingUser) {
      return res.status(400).json({ message: 'Uporabnik s temi podatki že obstaja' });
    }

    // Šifriranje gesla s pomočjo bcrypt
    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = new User({ firstname, lastname, username, password: hashedPassword, email }); // Include firstname and lastname when creating new user
    await newUser.save();

    res.status(201).json({ message: 'Uporabnik uspešno registriran' });
  } catch (error) {
    console.error('Napaka pri registraciji uporabnika:', error);
    res.status(500).json({ message: 'Napaka notranjega strežnika' });
  }
});

// Preverjanje prijave uporabnika
app.post('/api/login', async (req, res) => {
  const { username, password } = req.body;

  try {
      const user = await User.findOne({ username });

      if (user) {
          // Preveri ujemanje gesla s pomočjo bcrypt
          const isPasswordValid = await bcrypt.compare(password, user.password);

          if (isPasswordValid) {
              // Če uporabnik obstaja in je geslo pravilno, generiraj JWT žeton
              const accessToken = jwt.sign({ id: user._id, username: user.username, role: user.role }, accessTokenSecret, { expiresIn: accessTokenLife });
              const refreshToken = generateRefreshToken(user.username);
              res.json({ accessToken, refreshToken, userId: user._id }); // Dodamo userId v odgovor
          } else {
              // Če geslo ni pravilno, vrni napako
              res.status(401).json({ message: 'Napačno uporabniško ime ali geslo.' });
          }
      } else {
          // Če uporabnik ne obstaja, vrni napako
          res.status(401).json({ message: 'Napačno uporabniško ime ali geslo.' });
      }
  } catch (error) {
      // Če pride do napake pri poizvedbi, vrni napako
      console.error('Napaka pri prijavi uporabnika:', error);
      res.status(500).json({ message: 'Napaka pri prijavi uporabnika.' });
  }
});

// Končna točka za ponastavitev gesla
app.post('/api/reset-password', async (req, res) => {
  const { email, newPassword } = req.body;

  if (!email || !newPassword) {
    return res.status(400).json({ message: 'Manjka e-pošta ali novo geslo' });
  }

  if (newPassword.length < 8) {
    return res.status(400).json({ message: 'Geslo mora biti dolgo vsaj 8 znakov' });
  }

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(404).json({ message: 'Uporabnik s tem e-poštnim naslovom ne obstaja' });
    }

    const hashedPassword = await bcrypt.hash(newPassword, 10);
    user.password = hashedPassword;
    await user.save();

    res.status(200).json({ message: 'Geslo uspešno ponastavljeno' });
  } catch (error) {
    console.error('Napaka pri ponastavitvi gesla:', error);
    res.status(500).json({ message: 'Napaka notranjega strežnika' });
  }
});

// Funkcija za generiranje osveževalnega žetona
function generateRefreshToken(username) {
  return jwt.sign({ username: username }, refreshTokenSecret, { expiresIn: refreshTokenLife });
}

// Funkcija za posodobitev dostopnega žetona
function refreshAccessToken(req, res) {
  const refreshToken = req.body.refreshToken;
  if (!refreshToken) {
    return res.sendStatus(401);
  }
  jwt.verify(refreshToken, refreshTokenSecret, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }
    const accessToken = jwt.sign({ username: user.username, role: user.role }, accessTokenSecret, { expiresIn: accessTokenLife });
    res.json({ accessToken });
  });
}

// Končna točka za osvežitev dostopnega žetona
app.post('/api/token', refreshAccessToken);

// Avtentikacijski middleware za preverjanje dostopnega žetona
function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (!token) return res.sendStatus(401);

  jwt.verify(token, accessTokenSecret, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
}

// Endpoint to get user data by user ID
app.get('/api/user/:userId', authenticateToken, async (req, res) => {
  const { userId } = req.params;

  try {
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ user });
  } catch (error) {
    console.error('Error fetching user data:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
});

// Endpoint to update user data by user ID
app.put('/api/user/:userId', authenticateToken, async (req, res) => {
  const { userId } = req.params;
  const { email, firstname, lastname } = req.body;

  try {
    const updatedUser = await User.findByIdAndUpdate(userId, { email, firstname, lastname }, { new: true });

    if (!updatedUser) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ message: 'User data successfully updated', user: updatedUser });
  } catch (error) {
    console.error('Error updating user data:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
});

// Endpoint to delete user by user ID
app.delete('/api/user/:userId', authenticateToken, async (req, res) => {
  const { userId } = req.params;

  try {
    const deletedUser = await User.findByIdAndDelete(userId);

    if (!deletedUser) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ message: 'User successfully deleted' });
  } catch (error) {
    console.error('Error deleting user:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
});

// Končna točka za spremembo gesla
app.post('/api/change-password', authenticateToken, async (req, res) => {
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword || !newPassword) {
    return res.status(400).json({ message: 'Manjka staro ali novo geslo' });
  }

  if (newPassword.length < 8) {
    return res.status(400).json({ message: 'Novo geslo mora biti dolgo vsaj 8 znakov' });
  }

  try {
    const userId = req.user.id;
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'Uporabnik ni bil najden' });
    }

    // Preverimo ujemanje staro geslo
    const isPasswordValid = await bcrypt.compare(oldPassword, user.password);

    if (!isPasswordValid) {
      return res.status(401).json({ message: 'Napačno staro geslo' });
    }

    // Posodobimo geslo s šifriranjem novega gesla
    const hashedNewPassword = await bcrypt.hash(newPassword, 10);
    user.password = hashedNewPassword;
    await user.save();

    res.status(200).json({ message: 'Geslo uspešno spremenjeno' });
  } catch (error) {
    console.error('Napaka pri spremembi gesla:', error);
    res.status(500).json({ message: 'Napaka notranjega strežnika' });
  }
});

// Vračanje ID uporabnika glede na vnesen email
app.get('/api/users/email/:email', async (req, res) => {
  const { email } = req.params;
  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    res.json({ _id: user._id });
  } catch (error) {
    res.status(500).json({ message: 'Error retrieving user', error });
  }
});

// Zagon strežnika
app.listen(PORT, () => {
  console.log(`Strežnik deluje na http://localhost:${PORT}/`);
});