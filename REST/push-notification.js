const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const webpush = require('web-push');
const mongoose = require('mongoose');

const app = express();
const PORT = process.env.PORT || 4000;

app.use(cors());
app.use(bodyParser.json());

// Konfiguracija za web-push
const vapidKeys = webpush.generateVAPIDKeys();

webpush.setVapidDetails(
  'mailto:gorenjak.school@gmail.com',
  vapidKeys.publicKey,
  vapidKeys.privateKey
);

console.log('Javni ključ za push obvestila:', vapidKeys.publicKey);
console.log('Zasebni ključ za push obvestila:', vapidKeys.privateKey);

// Funkcija za pošiljanje push obvestil
async function sendPushNotification(subscription, dataToSend) {
  try {
    await webpush.sendNotification(subscription, JSON.stringify(dataToSend));
    console.log('Push notification sent successfully.');
  } catch (error) {
    console.error('Error sending push notification:', error);
  }
}

// Končna točka za pridobitev javnega ključa
app.get('/api/publicKey', (req, res) => {
    res.json({ publicKey: vapidKeys.publicKey });
  });
  
  // Končna točka za pošiljanje push obvestil
  app.post('/api/push/send', async (req, res) => {
    const { subscription, title, body } = req.body;
    if (!subscription || !title || !body) {
      return res.status(400).json({ message: 'Missing subscription, title, or body in request.' });
    }
  
    await sendPushNotification(subscription, { title, body });
    res.json({ success: true, message: 'Push notification sent successfully' });
  });
  
  // Končna točka za naročanje na push obvestila
  app.post('/api/push/subscribe', async (req, res) => {
    const subscription = req.body.subscription;
    if (!subscription) {
      return res.status(400).json({ message: 'Missing subscription in request.' });
    }
  
    // Tukaj lahko shranite naročilo v vašo bazo podatkov, če je potrebno
  
    res.json({ success: true, message: 'Push subscription successful' });
  });

  // Zagon strežnika
app.listen(PORT, () => {
    console.log(`Strežnik deluje na http://localhost:${PORT}/`);
  });