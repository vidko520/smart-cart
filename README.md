# Smart Cart

Flow dela: Za vsak nov feature naredimo novi branch. Po končanem delu na branchu naredimo Merge request v katerem se pregleda ali je vsa koda OK in se zmerga v main. Ne pozabi na git pull komando, da imaš vedno najnovejšo različico.

## Getting started

```
cd existing_repo
git branch
git checkout -b ime-brancha
git add
git status
git commit -m "this commit will.."
git push
```
