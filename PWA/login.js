const loginForm = document.getElementById('login-form');
const forgotPasswordLink = document.getElementById('forgot-password-link');
const resetPasswordModal = document.getElementById('reset-password-modal');
const resetPasswordForm = document.getElementById('reset-password-form');
const successModal = document.getElementById('success-modal');
const successClose = document.querySelector('.success-close');
const successButton = document.getElementById('success-button');

// Prikaz modalnega okna za ponastavitev gesla
forgotPasswordLink.onclick = function() {
  resetPasswordModal.style.display = 'block';
}

// Zapiranje modalnega okna za ponastavitev gesla
document.querySelector('.modal .close').onclick = function() {
  resetPasswordModal.style.display = 'none';
}

// Zapiranje modalnega okna za uspešno posodobitev gesla
successClose.onclick = function() {
  successModal.style.display = 'none';
}

// Zapiranje modalnega okna za uspešno posodobitev gesla ob kliku na gumb "V redu"
successButton.onclick = function() {
  successModal.style.display = 'none';
}

// Zapiranje modalnega okna, če uporabnik klikne izven njega
window.onclick = function(event) {
  if (event.target == resetPasswordModal) {
    resetPasswordModal.style.display = 'none';
  }
  if (event.target == successModal) {
    successModal.style.display = 'none';
  }
}

  // Pridobivanje javnega ključa za push obvestila iz strežnika
  async function getPublicKey() {
    try {
      const response = await fetch('http://localhost:4000/api/publicKey');
      const data = await response.json();
      return data.publicKey;
    } catch (error) {
      console.error('Error fetching public key:', error);
      return null;
    }
  }

  const publicKeyPromise = getPublicKey();

  function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');

    const binaryString = window.atob(base64);
    const bytes = new Uint8Array(binaryString.length);

    for (let i = 0; i < binaryString.length; ++i) {
      bytes[i] = binaryString.charCodeAt(i);
    }

    return bytes;
  }

  // Dodajanje kode za registracijo service workera
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
        // Naročilo na push obvestila
        publicKeyPromise.then(publicKey => {
          if (publicKey) {
            registerPushNotification(publicKey);
          } else {
            console.error('Failed to get public key');
          }
        });
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  async function registerPushNotification(publicKey) {
    try {
      const registration = await navigator.serviceWorker.ready;
      const subscription = await registration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(publicKey)
      });
      
      // Pošiljanje naročila na push obvestila na strežnik
      await fetch('http://localhost:4000/api/push/subscribe', {
        method: 'POST',
        body: JSON.stringify({ subscription }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
        }
      });

      console.log('Push subscription successful');
    } catch (error) {
      console.error('Push subscription failed:', error);
    }
  }

  loginForm.addEventListener('submit', async function(event) {
    event.preventDefault();

    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    const response = await fetch('http://localhost:3000/api/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ username, password })
    });

    if (response.ok) {
      const data = await response.json();
      localStorage.setItem('accessToken', data.accessToken);
      localStorage.setItem('refreshToken', data.refreshToken);
      localStorage.setItem('userId', data.userId); // Shranite ID uporabnika

      // Pošljemo zahtevo za pošiljanje potisnega sporočila
      sendPushNotification('Uspešna prijava', 'Prijavili ste se v sistem!');

      window.location.href = 'index.html';
    } else {
      // Prikažemo sporočilo o napaki ob napačnih prijavnih podatkih
      document.getElementById('error-message').style.display = 'block';
    }
  });

  async function sendPushNotification(title, body) {
    try {
      const registration = await navigator.serviceWorker.ready;

      // Prikaz potisnega obvestila v brskalniku
      await registration.showNotification(title, { body });

      console.log('Push notification sent successfully');
    } catch (error) {
      console.error('Error sending push notification:', error);
    }
  }

resetPasswordForm.addEventListener('submit', async function(event) {
  event.preventDefault();

  const email = document.getElementById('reset-email').value;
  const newPassword = document.getElementById('reset-new-password').value;
  const confirmPassword = document.getElementById('reset-confirm-password').value;

  if (newPassword !== confirmPassword) {
    document.getElementById('reset-error-message').innerText = 'Gesli se ne ujemata.';
    document.getElementById('reset-error-message').style.display = 'block';
    return;
  }

  const response = await fetch('http://localhost:3000/api/reset-password', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ email, newPassword })
  });

  if (response.ok) {
    resetPasswordModal.style.display = 'none';
    successModal.style.display = 'block';
  } else {
    const data = await response.json();
    document.getElementById('reset-error-message').innerText = data.message || 'Napaka pri ponastavitvi gesla.';
    document.getElementById('reset-error-message').style.display = 'block';
  }
});

// Funkcija za preverjanje, ali je element v vidnem polju
function isElementInViewport(el) {
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

// Funkcija za leno nalaganje slik
function lazyLoadImages() {
  const images = document.querySelectorAll('img[data-src]'); // Izberemo vse slike z atributom data-src

  images.forEach(image => {
    if (isElementInViewport(image)) {
      image.src = image.dataset.src; // Nalaganje slike iz podatkovnega atributa
      image.onload = function() {
        image.classList.add('loaded'); // Dodajanje razreda po nalaganju slike
      };
      image.removeAttribute('data-src'); // Odstranimo atribut data-src po nalaganju
    }
  });
}

// Ob posodobitvi strani ali premiku
document.addEventListener('DOMContentLoaded', lazyLoadImages);
window.addEventListener('scroll', lazyLoadImages);
window.addEventListener('resize', lazyLoadImages);