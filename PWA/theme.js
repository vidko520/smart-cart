let isColorChanged = false; // Variable to track color change state
const originalBackgroundColor = document.body.style.backgroundColor; // Store original background color
const originalTextColor = document.body.style.color; // Store original text color

// Function to handle color change
function toggleColor() {
  const productImage = document.querySelectorAll('.SmartCart-image');
  const modalContents = document.querySelectorAll('.modal-content'); // Select all modal content elements
  
  if (!isColorChanged) {
    document.body.style.backgroundColor = '#333'; // Change background color to gray
    document.body.style.color = '#fff'; // Change text color to white
    productImage.forEach(img => { 
      img.src = 'assets/SmartCartWhite.png'; // Change image source to SmartCartWhite.png
    });
    
    // Loop through each modal content and update styles
    modalContents.forEach(modalContent => {
      modalContent.style.backgroundColor = '#333'; // Change modal background color to gray
      modalContent.style.color = '#fff'; // Change modal text color to white
    });
    
    isColorChanged = true; // Update color change state
  } else {
    document.body.style.backgroundColor = originalBackgroundColor; // Revert background color
    document.body.style.color = originalTextColor; // Revert text color
    
    productImage.forEach(img => {
      img.src = 'assets/SmartCart.png'; // Change image source back to SmartCart.png
    });
    
    // Loop through each modal content and revert styles
    modalContents.forEach(modalContent => {
      modalContent.style.backgroundColor = '#fefefe'; // Revert modal background color
      modalContent.style.color = '#000'; // Revert modal text color
    });
    
    isColorChanged = false; // Update color change state
  }
}

// Dodamo dogodek za prepoznavanje kombinacije tipk Alt + D
document.addEventListener('keydown', function(event) {
  if (event.altKey && event.key === 'g') {
    toggleColor();
  }
});