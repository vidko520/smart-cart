      // Funkcija za prikaz modala
      function showModal(message, redirect = false) {
        const modal = document.getElementById('modal');
        const modalMessage = document.getElementById('modal-message');
        const closeButton = document.querySelector('.close');
        const okButton = document.getElementById('modal-ok-button');

        modalMessage.textContent = message;
        modal.style.display = 'block';

        // Zapri modal ob kliku na X
        closeButton.onclick = function() {
          modal.style.display = 'none';
          if (redirect) {
            window.location.href = 'login.html';
          }
        }

        // Zapri modal ob kliku na OK
        okButton.onclick = function() {
          modal.style.display = 'none';
          if (redirect) {
            window.location.href = 'login.html';
          }
        }

        // Zapri modal ob kliku izven modala
        window.onclick = function(event) {
          if (event.target == modal) {
            modal.style.display = 'none';
            if (redirect) {
              window.location.href = 'login.html';
            }
          }
        }
      }

      // Funkcija za registracijo uporabnika
      const registerForm = document.getElementById('register-form');

      registerForm.addEventListener('submit', async function(event) {
        event.preventDefault();

        const firstname = document.getElementById('firstname').value;
        const lastname = document.getElementById('lastname').value;
        const email = document.getElementById('email').value;
        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;
        const confirmPassword = document.getElementById('confirm-password').value;

        // Preverjanje, ali se gesli ujemata
        if (password !== confirmPassword) {
          showModal('Gesli se ne ujemata.');
          return;
        }

        try {
          const response = await fetch('http://localhost:3000/api/register', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ firstname, lastname, email, username, password })
          });

          const responseData = await response.json(); // Parse the JSON response

          if (response.ok) {
            // Prikaži modal z obvestilom o uspešni registraciji in preusmeritvijo
            showModal('Uporabnik uspešno registriran.', true);
          } else {
            // Prikaži modal z obvestilom o napaki
            if (responseData.message) {
              showModal(responseData.message); // Display error message from server
            } else {
              showModal('Prišlo je do napake pri registraciji.'); // Default error message
            }
          }
        } catch (error) {
          showModal('Prišlo je do napake pri registraciji.');
        }
      });

      // Funkcija za preverjanje, ali je element v vidnem polju
      function isElementInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
          rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
      }

      // Funkcija za leno nalaganje slik
      function lazyLoadImages() {
        const images = document.querySelectorAll('img[data-src]'); // Izberemo vse slike z atributom data-src

        images.forEach(image => {
          if (isElementInViewport(image)) {
            image.src = image.dataset.src; // Nalaganje slike iz podatkovnega atributa
            image.onload = function() {
              image.classList.add('loaded'); // Dodajanje razreda po nalaganju slike
            };
            image.removeAttribute('data-src'); // Odstranimo atribut data-src po nalaganju
          }
        });
      }

      // Ob posodobitvi strani ali premiku
      document.addEventListener('DOMContentLoaded', lazyLoadImages);
      window.addEventListener('scroll', lazyLoadImages);
      window.addEventListener('resize', lazyLoadImages);